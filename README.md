# HutchMonitor

## Check recorded data

1. To access the server, run the following in Terminal, followed by the password.
```
ssh s01.local
```
2. Safely unmount the external hard drive. It is worth checking how to the drive is mounted by running `df -h` first.
```
sudo umount /dev/sda1
```
3. Plug the drive into the laptop and check its contents. Then unmount the drive and plug it back into the server.